package com.oauthclient

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class AnnotationController {

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def index() {

    }

    def demo1() { }
    def demo2() { }
    def demo3() { }
    def demo4() { }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def demo5() { }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def demo6() { }

    def demo7() {
        render(view:  "demo6")
    }


    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def demo8() { }

}
