package com.oauthclient

import com.nimbusds.jose.JOSEObjectType
import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSObject
import com.nimbusds.jose.JWSSigner
import com.nimbusds.jose.Payload
import com.nimbusds.jose.crypto.MACSigner
import com.nimbusds.jwt.JWTClaimsSet

class AuthController {

    private static final String SHARED_KEY = "{shared key}";

    def grailsApplication

    def index() {}

    /**
     * Endpoint used by client to generate token
     *
     * @see https://github.com/okfn/annotator/wiki/Authentication
     * @return
     */
    def token() {
        println "Get token: " + params
        render(status: 200, text: getToken())
    }

    /**
     * Generate a token for the client
     *
     * @return
     */
    def getToken() {

        log.info "Generate token using userId=${grailsApplication.config.annotator.auth.userId}, consumerKey=${grailsApplication.config.annotator.auth.consumerKey}, " +
                "ttl=${grailsApplication.config.annotator.auth.ttl?:86400}"
        // Given a user instance
        // Compose the JWT claims set
        JWTClaimsSet jwtClaims = new JWTClaimsSet();
        jwtClaims.setIssueTime(new Date());
        jwtClaims.setJWTID(UUID.randomUUID().toString());
        //jwtClaims.setCustomClaim("userId", "jmiranda");
        //jwtClaims.setCustomClaim("consumerKey", "0cbfa370-b73c-4e3a-ae46-582df284b7c3");
        //jwtClaims.setCustomClaim("ttl", 86400);

        jwtClaims.setCustomClaim("userId", grailsApplication.config.annotator.auth.userId.toString());
        jwtClaims.setCustomClaim("consumerKey", grailsApplication.config.annotator.auth.consumerKey.toString());
        jwtClaims.setCustomClaim("ttl", 86400);
        jwtClaims.setCustomClaim("issuedAt", new Date().format("yyyy-MM-dd'T'hh:mm:ssZ")); // 2013-08-30T22:23:30+00:00

        log.info "JWTClaims: " + jwtClaims.toString()

        // jwtClaims.setCustomClaim("email", user.email);

        // Create JWS header with HS256 algorithm
        //JWSHeader
        JWSHeader header = new JWSHeader(JWSAlgorithm.HS256);
        header.setContentType("text/plain");
        header.setType(JOSEObjectType.JWS)

        // Create JWS object
        JWSObject jwsObject = new JWSObject(header, new Payload(jwtClaims.toJSONObject()));

        // Create HMAC signer

        JWSSigner signer = new MACSigner(SHARED_KEY.getBytes());

        try {
            jwsObject.sign(signer);
        } catch(com.nimbusds.jose.JOSEException e) {
            System.err.println("Error signing JWT: " + e.getMessage());
            return;
        }

        // Serialise to JWT compact form
        //String jwtString = jwsObject.serialize();
        return jwsObject.serialize()
    }

}
