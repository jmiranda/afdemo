package com.oauthclient

import org.scribe.model.Token

class FacebookController {

    def oauthService

    def index() {

//        if (!session.oauthToken) {
//            render "The token could no be retrieved."
//        }
//        else {
            //def requestToken = oauthService.fetchRequestToken('facebook')
            //render requestToken
            //render "hello world"
            //def accessToken = oauthService.fetchAccessToken('facebook', requestToken)
            //def response = oauthService.postTwitterResource(twitterAccessToken, 'http://api.url')
//        }
        Token oauthToken = session[oauthService.findSessionKeyForAccessToken('facebook')]
        if (!oauthToken) {
            render "You need to login using Facebook!"
        }
        else {
            def response = oauthService.accessResource('facebook', oauthToken, 'GET', 'https://graph.facebook.com/me')
            render response.body
        }
    }




}
