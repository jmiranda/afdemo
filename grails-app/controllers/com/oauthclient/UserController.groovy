package com.oauthclient

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class UserController {

    def scaffold = User
}
