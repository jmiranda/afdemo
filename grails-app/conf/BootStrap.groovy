import com.oauthclient.Role
import com.oauthclient.User
import com.oauthclient.UserRole

class BootStrap {

    def init = { servletContext ->

        // Used for OAuth
        javax.servlet.http.HttpServletRequest.metaClass.getAbsoluteRequestURI = {
            return (delegate.scheme + "://" + delegate.serverName + ":" + delegate.serverPort + delegate.getContextPath())
        }

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        def testUser1 = new User(username: 'jmiranda', enabled: true, password: 'password').save(flush: true)
        def testUser2 = new User(username: 'justin', enabled: true, password: 'password').save(flush: true)
        def testUser3 = new User(username: 'paolo', enabled: true, password: 'password').save(flush: true)
        def testUser4 = new User(username: 'daniel', enabled: true, password: 'password').save(flush: true)


        UserRole.create testUser1, adminRole, true
        UserRole.create testUser2, adminRole, true
        UserRole.create testUser3, adminRole, true
        UserRole.create testUser4, adminRole, true

        //assert User.count() == 1
        //assert Role.count() == 2
        //assert UserRole.count() == 1

    }
    def destroy = {
    }
}
