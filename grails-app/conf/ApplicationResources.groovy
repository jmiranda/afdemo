modules = {
    application {
        resource url:'js/application.js'
    }

    annotator {
        dependsOn 'jquery'
        resource url:'http://assets.annotateit.org/annotator/v1.2.5/annotator-full.min.js'
        resource url:'http://assets.annotateit.org/annotator/v1.2.5/annotator.min.css'
        resource url:'/css/openshakespeare.css'
    }

    //customBootstrap {
    //    dependsOn 'font-awesome'
    //    resource url: 'css/bootstrap.css'
    //    resource url: 'js/bootstrap.js'
    //    resource url: 'css/bootstrap-fixtaglib.css'
    //}

}