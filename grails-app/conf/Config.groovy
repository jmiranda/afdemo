// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

grails.config.locations = [ "file:./${appName}-config.properties",
                            "file:./${appName}-config.groovy",
                            "classpath:${appName}-config.properties",
                            "classpath:${appName}-config.groovy",
                            "file:${userHome}/.grails/${appName}-config.properties",
                            "file:${userHome}/.grails/${appName}-config.groovy" ]

if (System.properties["${appName}.config.location"]) {
    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
}

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
    all:           '*/*',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true

        // Annotator API
        //annotator.store.prefix = "http://localhost:8080/oauthserver/annotator"
        //annotator.auth.tokenUrl = "http://localhost:8080/oauthserver/annotator/token"
        //annotator.store.prefix = "http://localhost:8080/catch/annotator"
        //annotator.auth.tokenUrl = "http://localhost:8080/catch/annotator/token"
        //annotator.store.prefix = "http://162.242.217.241:8080/catch/annotator"
        //annotator.auth.tokenUrl = "http://162.242.217.241:8080/catch/annotator/token"


        //annotator.store.prefix = "http://catch.harvardx.harvard.edu/catch/annotator"
        //annotator.auth.tokenUrl = "http://catch.harvardx.harvard.edu/catch/annotator/token"

        //annotator.store.prefix = "http://23.251.152.195:8080/catch/annotator"
        //annotator.auth.tokenUrl = "http://23.251.152.195:8080/catch/annotator/token"

        annotator.store.prefix = "http://localhost:8080/catch/annotator"
        annotator.auth.tokenUrl = "http://localhost:8080/catch/annotator/token"
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"

        // Annotator API
        //annotator.store.prefix = "http://afstore.aws.af.cm/annotator"
        //annotator.auth.tokenUrl = "http://afstore.aws.af.cm/annotator/token"
        annotator.store.prefix = "http://catch.aws.af.cm/annotator"
        annotator.auth.tokenUrl = "http://catch.aws.af.cm/annotator/token"

    }
}

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'

    info    'grails.app',
            'grails.app.jobs',
            'org.liquibase',
            'org.springframework.security',
            'org.codehaus.groovy.grails.rest',
            'grails.plugin.databasemigration',
            'grails.plugins.httplogger',
            'org.springframework',
            'org.hibernate',
            'com.mchange',
            'grails.app.controller',
            'grails.app.bootstrap',
            'grails.app.service',
            'grails.app.task',
            'grails.plugin.springcache',
            'grails.plugin.springsecurity',
            'org.springframework.security',
            'BootStrap',
            'liquibase'
}


// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'com.oauthclient.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'com.oauthclient.UserRole'
grails.plugins.springsecurity.authority.className = 'com.oauthclient.Role'

// Added by the Spring Security OAuth plugin:
grails.plugins.springsecurity.oauth.domainClass = 'com.oauthclient.OAuthID'

oauth {
    debug = true
    providers {

        example {
            api = oauth.ExampleApi
            key = 'xrG1jWRBwMqmOOmbULCeHw'
            secret = 'FLhY86tr2XZZCWDn6S3mEVSWb2kS3fKa9xmUEXWQZk'
            //key = 'oauth_twitter_key'
            //secret = 'oauth_twitter_secret'
            successUri = '/oauth/example/success'
            failureUri = '/oauth/example/error'
            callback = "http://justinmiranda.com:8081/oauthclient/oauth/example/callback"

        }



        twitter {
            api = org.scribe.builder.api.TwitterApi
            key = 'xrG1jWRBwMqmOOmbULCeHw'
            secret = 'FLhY86tr2XZZCWDn6S3mEVSWb2kS3fKa9xmUEXWQZk'
            //key = 'oauth_twitter_key'
            //secret = 'oauth_twitter_secret'
            successUri = '/oauth/twitter/success'
            failureUri = '/oauth/twitter/error'
            callback = "http://justinmiranda.com:8081/oauthclient/oauth/twitter/callback"

        }
        facebook {
            api = org.scribe.builder.api.FacebookApi
            key = '536212399766080'
            secret = 'face808515b6083a673b8d1046276935'
            successUri = '/oauth/facebook/success'
            failureUri = '/oauth/facebook/error'
            callback = "http://justinmiranda.com:8081/oauthclient/oauth/facebook/callback"
        }

        linkedin {
            api = org.scribe.builder.api.LinkedInApi
            key = 'oauth_linkedin_key'
            secret = 'oauth_linkedin_secret'
            successUri = '/oauth/linkedin/success'
            failureUri = '/oauth/linkedin/error'
            callback = "http://localhost:8081/oauthclient/oauth/linkedin/callback"
        }
        google {
            api = org.scribe.builder.api.GoogleApi
            key = 'oauth_google_key'
            secret = 'oauth_google_secret'
            successUri = '/oauth/google/success'
            failureUri = '/oauth/google/error'
            callback = "https://localhost:8081/oauthclient/oauth/google/callback"
            scope = 'http://www.googleapis.com/auth/userinfo.email'
        }
    }
}
