<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
        <div class="logo" id="grailsLogo" role="banner" style="vertical-align:middle">
            <h1>
                <img src="${resource(dir:'images/shared', file: 'logo.resized.resized.png')}" alt="Catch Annotation Hub"
                 style="display:inline; vertical-align: middle;"/>
                <a href="${request.contextPath}">CATCH Annotation Demo</a>
            </h1>
        </div>

        <div class="nav">
            <ul>
                <li>
                    <g:link uri="/"><i class="icon-home"></i> Home</g:link>
                </li>
                <li>
                    <g:link controller="annotation" action="index">Demo</g:link>
                </li>
                <li>
                    <g:link controller="user" action="index">Users</g:link>
                </li>
                <sec:ifLoggedIn>
                    <li style="float: right;">

                        <g:link controller="logout">Log out</g:link>
                    </li>
                    <li style="float: right;">
                        <span>Logged in as <sec:username /></span>
                    </li>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li style="float: right;">

                        <g:link controller="login" action="auth"><i class="icon-lock"></i> Login</g:link>
                    </li>
                    <li style="float: right;">
                        <span>You are not logged in.</span>
                    </li>
                </sec:ifNotLoggedIn>
            </ul>
        </div>

		<g:layoutBody/>
		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
