<%@ page import="com.oauthclient.User" %>
<html>
<head>
	<meta name='layout' content='main'/>
	<title><g:message code="springSecurity.login.title"/></title>
	<style type='text/css' media='screen'>

	</style>
</head>

<body>

<%--
<div id="oauth">
    <oauth:connect provider="example" id="example-connect-link">Example</oauth:connect>
    <oauth:connect provider="twitter" id="twitter-connect-link">Twitter</oauth:connect>
    <oauth:connect provider="facebook" id="facebook-connect-link">Facebook</oauth:connect>
    <oauth:connect provider="google" id="google-connect-link">Google</oauth:connect>
    <oauth:connect provider="yahoo" id="yahoo-connect-link">Yahoo</oauth:connect>
</div>
--%>

<div id="status" role="complementary">
    <h1>Available Users</h1>
    <ul>
        <g:each in="${User.list()}" var="user">
            <li>${user.username} : password</li>
        </g:each>
    </ul>
</div>
<div id="page-body" role="main">
<div id='login'>
	<div class='inner'>
		<div class='fheader'><g:message code="springSecurity.login.header"/></div>

		<g:if test='${flash.message}'>
			<div class='login_message'>${flash.message}</div>
		</g:if>

		<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
			<p>
				<label for='username'><g:message code="springSecurity.login.username.label"/>:</label>
				<input type='text' class='text_' name='j_username' id='username'/>
			</p>

			<p>
				<label for='password'><g:message code="springSecurity.login.password.label"/>:</label>
				<input type='password' class='text_' name='j_password' id='password'/>
			</p>

			<p id="remember_me_holder">
				<input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
				<label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
			</p>

			<p>
				<input type='submit' id="submit" value='${message(code: "springSecurity.login.button")}'/>
			</p>

		</form>
	</div>
</div>
</div>
<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>
