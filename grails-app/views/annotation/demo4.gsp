
<!DOCTYPE html>
<html lang="en">

<html>
<head>
    <title>annotator.js demo</title>
    <meta name="layout" content="main"/>
    <r:require modules="annotator"/>
    <meta charset="utf-8" />
    <!-- Prevent the conditional comments from stalling page load -->
    <!--[if IE]><![endif]-->

    <title>Demo - Annotator - Annotating the Web</title>

    <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

    <link href='http://fonts.googleapis.com/css?family=Arvo:regular,bold&amp;subset=latin' rel='stylesheet' />
    <link href='http://fonts.googleapis.com/css?family=Inconsolata&amp;subset=latin' rel='stylesheet' />

    <link href="http://assets.annotateit.org/annotator/v1.2.7/annotator.min.css" rel="stylesheet" />

</head>
<body>

<div id="content">

    <div id="text-to-annotate">
        <header>
            <h1>
                <a href="../" title="The Annotator Homepage">Annotator</a>
            </h1>
            <nav>
                <ul>
                    <li><a href="http://github.com/okfn/annotator/" title="Check out the repository">Code</a></li>
                    <li><a href="http://github.com/okfn/annotator/downloads/" title="Download a packaged bundle">Download</a></li>
                    <li><a href="http://github.com/okfn/annotator/wiki/" title="Read the wiki">Wiki</a></li>
                    <li><a href="http://github.com/okfn/annotator/issues/" title="Report an issue">Issues</a></li>
                </ul>
            </nav>
        </header>
        <article>
            <section>
                <h2>The Annotator Demo</h2>
                <p>Annotator is a Javascript shim that you can insert into any page, allowing
                you to select and annotate text, images, or (almost) anything else.</p>

                <p><strong>How do I use it?</strong> We think you'll get the hang of it
                pretty quickly. It's running right now on this page &mdash; try it out
                by selecting some text and clicking on the note icon. Enter a note and
                press <tt>&ldquo;Enter &#x21A9;&rdquo;</tt>: you should see your annotation
                as a highlighted piece of text. Hover over the highlight with your mouse
                to view, edit and delete the annotation.</p>

                <p><strong>What happens to my annotations?</strong> The annotations on
                this page are being sent to
                    <a href="http://annotateit.org">AnnotateIt</a> an online web service
                for storing annotations. However it's simple to set up your own
                store. Check out the <a href="http://github.com/okfn/annotator/wiki/Storage">Storage</a>
                    and <a href="http://github.com/okfn/annotator/wiki/Annotation-format">Annotation Format</a>
                    wiki pages for more details.</p>

                <p><strong>Give me the code!</strong> Right, go get a tagged release from
                    <a href="https://github.com/okfn/annotator/downloads">this page</a>,
                load <code>annotator.min.js</code> and <code>annotator.min.css</code> into
                your page, and add the following Javascript<sup><a href="#jquery">1</a>
                </sup>:</p>

                <pre><code>$('body').annotator()</code></pre>

                <p>If that doesn't make any sense to you, then go see the wiki page on <a href="http://github.com/okfn/annotator/wiki/Getting-Started">Getting Started</a> for more information.</p>

                <p class="footnote">
                    <a name="jquery">1</a>: I'm assuming you're already using <a href="http://jquery.com">jQuery</a>, right?
                If not you'll need to load that too. Best use Google's CDN with a URL like
                    <code>http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js</code>.
                </p>
            </section>
        </article>
        <footer>
            <p>Annotator was developed by the
                <img src="http://assets.okfn.org/images/logo/okf_logo_white_and_green_tiny.png" id="footer-okf-logo" />
                <a href="http://okfn.org">Open
                Knowledge Foundation</a> with support from the <a
                    href="http://www.shuttleworthfoundation.org/">Shuttleworth
                Foundation</a>. Go see what other cool stuff we're doing at the
            moment!</p>
            <div class="widget-area">
                <ul class="xoxo">
                    <li class="widget-container widget_text">
                        <h3 class="widget-title"></h3>
                        <div class="text-widget">
                            <ul>
                                <li>
                                &copy; <a href="http://www.okfn.org/">Open Knowledge Foundation</a>
                                </li>
                                <li>
                                    <a href="http://opendefinition.org/okd/" class="img">
                                        <img alt="This Content and Data is Open" src="http://assets.okfn.org/images/ok_buttons/oc_80x15_blue.png" class="button" />
                                    </a>
                                </li>
                                <li>
                                    <a href="http://opendefinition.org/ossd/" class="img">
                                        <img alt="This Content and Data is Open"
                                             src="http://assets.okfn.org/images/ok_buttons/os_80x15_orange_grey.png" class="button" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <ul class="xoxo">
                    <li class="widget-container widget_text">
                        <h3 class="widget-title"></h3>
                        <div class="textwidget">
                            <ul>
                                <li>
                                    <a href="http://okfn.org/projects/annotator/">Project Home Page</a>
                                </li>
                                <li>
                                    <a href="/api/">API</a>
                                </li>
                                <li>
                                    <a href="http://github.com/okfn/annotator-store/">API Docs</a>
                                </li>
                                <li>
                                    <a href="http://lists.okfn.org/mailman/listinfo/annotator-dev">Mailing List</a>
                                </li>
                                <li>
                                    <a href="http://okfn.org/privacy-policy/">Privacy Policy</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </footer>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="${resource(dir:'js', file:'jwt.js')}"></script>
<script src="http://assets.annotateit.org/annotator/v1.2.7/annotator-full.min.js"></script>
<%--
<script src="${resource(dir:'js', file:'main.js')}j"></script>
--%>
<script src="${resource(dir:'js', file:'jwt.js')}"></script>

<script>
    if (!Date.prototype.toISOString) {
        Date.prototype.toISOString = function() {
            function pad(n) { return n < 10 ? '0' + n : n; }
            return this.getUTCFullYear() + '-'
                    + pad(this.getUTCMonth() + 1) + '-'
                    + pad(this.getUTCDate()) + 'T'
                    + pad(this.getUTCHours()) + ':'
                    + pad(this.getUTCMinutes()) + ':'
                    + pad(this.getUTCSeconds()) + 'Z';
        };
    }

    // Please note that you would *never* expose the consumer secret in this way
    // for an ordinary application. For the purposes of convenience for this demo, we do.
    //var demoConsumerKey = 'd4c108122b51434aab1d27ad4ebd2b02';
    //var demoConsumerSecret = '36977e7b-be7f-4b57-a9eb-9617e4740b6a';

    var demoConsumerKey = "80404495-7196-4879-8719-54c21f44a31a";
    var demoConsumerSecret ="this doesn't matter yet as we don't validate against the secret"

    function getAuthToken(consumerKey, consumerSecret, userId) {
        "use strict";

        var date = new Date(new Date() - 1 * 60 * 60 * 1000), // 1 hour ago
                alg = { typ:'JWT', alg:'HS256' },
                payload = {
                    consumerKey: consumerKey,
                    userId: userId,
                    issuedAt: date.toISOString(),
                    ttl: 86400
                },
                token;

        alg = JSON.stringify(alg);
        payload = JSON.stringify(payload);
        token = new jwt.WebToken(payload, alg);

        return token.serialize(consumerSecret);
    }

    function initAnnotator(json) {
        "use strict";


        $('article').annotator().annotator('setupPlugins', null, {

            Store: {
                prefix: '${grailsApplication.config.annotator.store.prefix}',
                // Attach the uri of the current page to all annotations to allow search.
                annotationData: {
                    'uri': '${createLink(action: action, controller: controller, id: id, absolute:true)}'
                },
                urls: {
                    // These are the default URLs.
                    create:  '/create',
                    read:    '/read/:id',
                    update:  '/update/:id',
                    destroy: '/destroy/:id',
                    search:  '/search'
                },
                // This will perform a "search" action rather than "read" when the plugin
                // loads. Will request the last 20 annotations for the current url.
                // eg. /store/endpoint/search?limit=20&uri=http://this/document/only
                loadFromSearch: {
                    'limit': 20,
                    'uri': '${createLink(action: action, controller: controller, id: id, absolute:true)}'
                }

            },
            Auth: {
                token: getAuthToken(demoConsumerKey, demoConsumerSecret, json.ip)
                //tokenUrl: '${request.contextPath}/auth/token'
            },
            Permissions: false,
            AnnotateItPermissions: {}
        });

        $('article').annotator('addPlugin', 'Permissions', {
            user: '<sec:loggedInUserInfo field="username"/>'
        });

    }
</script>
<script type="application/javascript" src="http://jsonip.com/initAnnotator"></script>
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-8271754-20']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
</body>
</html>
