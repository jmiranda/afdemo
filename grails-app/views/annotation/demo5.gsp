<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>annotateme demo</title>
    <meta name="layout" content="main"/>
    <r:require modules="annotator"/>
</head>
<body>
<div id="content">
    <h1>Demo</h1>

    <div id="text-to-annotate">
    <p>
        Lorem ipsum dolor sit amet, et per alii placerat patrioque, per habeo quando an, vix iisque appareat volutpat at. Admodum eligendi erroribus ex eum, eu probo facer eum! Vidit dicant pro no, mel et malis appetere. Facilisis dignissim ad qui. Idque graeco scaevola duo in, vix mazim admodum suscipiantur ad.
    </p>
    <p>
        No cum cetero menandri democritum, sonet dicunt sea in, eu mel posse elaboraret honestatis? Liber repudiandae cu est, veri prima postea at sea, erant adversarium pro ut. Sit eros ullum ex! Veritus nusquam at nec, ea aeterno necessitatibus est, sea vero zril iisque et. Nam ei diam pericula, ne nec reque eleifend. Eu meliore docendi deserunt eos, discere eligendi incorrupte ex sit.
    </p>
    <p>
        Ut vim duis nihil, utamur fabellas qui ne, dolore nostrum constituam sea eu. Sit zril contentiones consectetuer at! Id rebum eruditi delicatissimi sea? Cibo animal id his, ei admodum volutpat dignissim qui. In mel utinam consul, has dicta ridens percipitur ex.
    </p>
    <p>
        Ea natum dolorem tibique duo, id sed etiam mazim. Ius nullam antiopam constituto te, his ei habeo expetenda reformidans! Possit menandri id usu, laoreet eligendi ocurreret no mea. Brute saepe vis ei. Eos no dicta meliore, percipit referrentur et vel, id qui percipit platonem! Sea eu fabulas veritus patrioque, vivendum quaerendum ut mei.
    </p>
    <p>
        Et nec lorem apeirian partiendo, quo in munere noster audiam, cu est tota virtute feugait. In mei quodsi inimicus, nam ne illud bonorum explicari, cibo sale repudiandae mea ne. In eos cibo ornatus, ius ei suas sensibus conceptam! Ut probo debet suavitate vim, choro contentiones conclusionemque his et! Vidit inimicus quo ex.
    </p>
    <p>
        Populo quidam in his. His augue tritani accusam ex, te errem aperiri suscipiantur mea, usu dolore repudiare mnesarchum id. Id cum volutpat expetenda definitiones. Graece altera cu eam.
    </p>
    <p>
        Dicunt tritani ea usu. Pro quot assentior no, quis dolore in vel! Justo delicatissimi quo cu. In vis habeo suscipit. Ne quo facer liber delectus, quot inermis no mea!
    </p>
    <p>
        Id per senserit explicari? Cu vix lorem summo inciderint, facilis accommodare id nec? Ius id probo autem facilisi, omnes imperdiet qui no? Quo essent fabulas mediocrem ex, in alia tollit duo.
    </p>
    <p>
        Sea fabulas evertitur cu! Pri in dicant convenire definiebas, partem omittantur vix no, accusata honestatis ad sed. Vix in dicam partiendo, vis cu verear pericula concludaturque! Ius electram torquatos interpretaris et, sed bonorum accusam ei, an dolore partiendo duo! Ne per primis euismod eligendi. Duo cu novum accumsan perfecto, ius te idque facer verterem, duo in nibh dicunt eripuit.
    </p>
    <p>
        Doming ponderum scripserit ex mel? Mel fabulas inciderint ex, at per quando primis euismod. Ne quodsi labitur ullamcorper duo, amet contentiones duo id. Verear saperet singulis te has, aperiri perpetua et mel, quaerendum referrentur sit ad. Prima conceptam sit no?
    </p>
    <p>
        Vitae libris laboramus mel cu, ad numquam tibique pro? Duis meliore his ei, menandri antiopam ea quo. Adhuc persecuti ut nec, ad vidisse eripuit minimum vim. Eum vivendo assentior eu.
    </p>
    <p>
        Dolores argumentum ex mel, at his vocent numquam scribentur, quidam nusquam elaboraret sit ex. Sed ut ipsum debitis tractatos, ne vidisse platonem mea. Te cum justo molestie referrentur. Elit eligendi qui ex, pro an option inermis sapientem! Has te ceteros recusabo!
    </p>
    <p>
        Eirmod volutpat efficiantur vix at. Prompta blandit laboramus qui ea, nec audire singulis urbanitas te! Eum et facilis voluptatum, exerci impetus vivendo ut vim. Nam ad iriure commodo qualisque, mel ex rebum ignota argumentum. Eu moderatius signiferumque nam, mei sumo dolorem eu, duis eripuit sensibus ei nam?
    </p>
    <p>
        Pri falli affert labore et, ex vel vide utinam democritum! Vix fugit quaeque ornatus id. Sed ea eius dicunt placerat, mollis posidonium vel ne, adhuc insolens quo ad. Ius at meliore senserit postulant, cu nibh aeque legimus eos. Ne sed cibo libris!
    </p>
    <p>
        Nec at nonumy petentium. Ex pro lorem maiestatis. Dicit iudico tacimates ex eum, fabulas sapientem repudiare sed cu. Summo nulla dolores cum ut, at quis epicuri offendit quo. Fuisset repudiare vis ea, te sea graecis posidonium.
    </p>
    </div>
</div>
<script>
    $(function () {

        var content = $('#content').annotator();

        content.annotator('addPlugin', 'Auth', {
            //tokenUrl: '${grailsApplication.config.annotator.auth.tokenUrl}'
            tokenUrl: '${request.contextPath}/auth/token'
        });


        content.annotator('addPlugin', 'Store', {
            // The endpoint of the store on your server.
            //prefix: '/store/endpoint',
            //prefix: 'http://afstore.aws.af.cm/annotator',
            //grailsApplication.config
            prefix: '${grailsApplication.config.annotator.store.prefix}',
            // Attach the uri of the current page to all annotations to allow search.
            annotationData: {
                'uri': '${createLink(action: action, controller: controller, id: id, absolute:true)}'
            },
            urls: {
                // These are the default URLs.
                create:  '/create',
                read:    '/read/:id',
                update:  '/update/:id',
                destroy: '/destroy/:id',
                search:  '/search'
            },
            // This will perform a "search" action rather than "read" when the plugin
            // loads. Will request the last 20 annotations for the current url.
            // eg. /store/endpoint/search?limit=20&uri=http://this/document/only
            loadFromSearch: {
                'limit': 20,
                'uri': '${createLink(action: action, controller: controller, id: id, absolute:true)}'
            }
        });
        content.annotator('addPlugin', 'Tags');
        //content.annotator('addPlugin', 'Filter', {
        //    filters: [
        //        {
        //            label: 'Quote',
        //            property: 'quote'
        //        }
        //    ]});
        //content.annotator('addPlugin', 'Markdown');
        //content.annotator('addPlugin', 'Permissions', {
        //    user: '<sec:loggedInUserInfo field="username"/>'
        //});

    });
</script>
</body>
</html>

