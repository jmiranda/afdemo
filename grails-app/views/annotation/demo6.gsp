<%--
  Created by IntelliJ IDEA.
  User: jmiranda
  Date: 9/3/13
  Time: 9:39 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JS annotation test</title>

    <!-- Annotator -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script src="http://assets.annotateit.org/annotator/v1.2.7/annotator-full.min.js"></script>
    <link rel="stylesheet" href="http://assets.annotateit.org/annotator/v1.2.7/annotator.min.css">

    <!--video-js-->
    <link href="http://vjs.zencdn.net/4.1/video-js.css" rel="stylesheet">
    <script src="${resource(dir: 'js/video-js', file: 'video.min.js')}"></script>

    <!--Youtube Pluging-->
    <script src="${resource(dir: 'js/video-js', file: 'vjs.youtube.js')}"></script>

    <!--RangeSlider Pluging-->
    <script src="${resource(dir: 'js/', file: 'rangeslider.min.js')}"></script>
    <link href="${resource(dir: 'css/', file: 'rangeslider.min.css')}" rel="stylesheet">

    <!--Share Pluging-->
    <script src="${resource(dir: 'js/', file: 'share-annotator.min.js')}"></script>
    <link href="${resource(dir: 'css/', file: 'share-annotator.min.css')}" rel="stylesheet">

    <!--Geolocation Pluging-->
    <script src="${resource(dir: 'js/', file: 'geolocation-annotator.min.js')}"></script>
    <link href="${resource(dir: 'css/', file: 'geolocation-annotator.min.css')}" rel="stylesheet">

    <!--RichText Pluging-->
    <script src="${resource(dir: 'js/tinymce', file: 'tinymce.min.js')}"></script>
    <script src="${resource(dir: 'js/', file: 'richText-annotator.min.js')}"></script>
    <link href="${resource(dir: 'css/', file: 'richText-annotator.min.css')}" rel="stylesheet">

    <!--OpenVideoAnnotations Pluging-->
    <script src="${resource(dir: 'js/', file: 'ova.min.js')}"></script>
    <link href="${resource(dir: 'css/', file: 'ova.min.css')}" rel="stylesheet">

    <!--Demo CSS-->
    <link href="${resource(dir: 'css/', file: 'demo.css')}" rel="stylesheet">
</head>

<body style="margin:10px">
<header>
    <h1>Javascript annotation service test</h1>
</header>

<div id="controls">
    <h3>Controls</h3>
    <p>
        <input id="username" value="Mark">
    </p>
</div>

<div id="airlock">
    <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>
    <video id="vid1" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"
           poster="http://video-js.zencoder.com/oceans-clip.png"
           data-setup=''>
        <source src="http://video-js.zencoder.com/oceans-clip.mp4" type='video/mp4' />
        <source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />
        <source src="http://video-js.zencoder.com/oceans-clip.ogv" type='video/ogg' />
        <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
    </video>
    <br />
    The same video, but with different annotations:
    <br />
    <video id="vid2" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"
           poster="http://video-js.zencoder.com/oceans-clip.png"
           data-setup=''>
        <source src="http://video-js.zencoder.com/oceans-clip.mp4" type='video/mp4' />
        <source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />
        <source src="http://video-js.zencoder.com/oceans-clip.ogv" type='video/ogg' />
        <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
    </video>
    <br />
    And you could use youtube video:
    <br />
    <video id="vid3" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"
           data-setup=''>
        <source src="http://www.youtube.com/watch?v=-m_S-IDs3Wg" type="video/youtube">
    </video>
</div>

<script>
    //Options to load in Open Video Annotation, for all the plugins
    var options = {
        optionsAnnotator: {
            user: { },
            auth: {
                tokenUrl: "${request.contextPath}/auth/token"
            },
            store: {
                // The endpoint of the store on your server.
                //prefix: 'http://afstore.aws.af.cm/annotator',
                prefix: 'http://catch.aws.af.cm/annotator',

                annotationData: {uri:'http://danielcebrian.com/annotations/demo.html'},

                urls: {
                    // These are the default URLs.
                    create:  '/create',
                    read:    '/read/:id',
                    update:  '/update/:id',
                    destroy: '/destroy/:id',
                    search:  '/search'
                },

                loadFromSearch:{
                    limit:10000,
                    uri: 'http://danielcebrian.com/annotations/demo.html'
                }
            }
        },
        optionsVideoJS: {techOrder: ["html5","flash","youtube"]},
        optionsRS: {},
        optionsOVA: {posBigNew:'none',NumAnnotations:20},
        optionsRichText: {
            tinymce:{
                selector: "li.annotator-item textarea",
                plugins: "media image insertdatetime link code",
                menubar: false,
                toolbar_items_size: 'small',
                extended_valid_elements : "iframe[src|frameborder|style|scrolling|class|width|height|name|align|id]",
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media rubric | code ",
            }
        }
    }
    //Load the plugin Open Video Annotation
    var ova = new OpenVideoAnnotation.Annotator($('#airlock'),options);
    ova.annotator.addPlugin('Auth', {
        //tokenUrl: 'http://catch.aws.af.cm/annotator/token'
        tokenUrl: '${request.contextPath}/auth/token'
        //autoFetch: true
     });

    //change the user (Experimental)
    ova.setCurrentUser($('#username').val());
    $('#username').change(function () {
        ova.setCurrentUser($(this).val());
    });
</script>
</body>


</html>
