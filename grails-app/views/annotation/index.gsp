<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>annotator.js demo</title>
    <meta name="layout" content="main"/>
    <%--

    <r:require module="customBootstrap" />
    --%>
    <r:require modules="annotator"/>


</head>
<body>
<div id="content">
    <h1>Demos</h1>
    <div id="text-to-annotate" style="width:40em;">
        <h2>Text Annotator</h2>
        <ul>
            <li>
                <g:link controller="annotation" action="demo5">Basic demo using Catch Storage API</g:link>
                (does not require authentication)

            </li>
            <li>
                <g:link controller="annotation" action="demo1">Basic demo using Catch Storage API</g:link>
                (token generated using tokenUrl = <g:link controller="auth" action="token">/auth/token</g:link>)

            </li>
            <%--
            <li><g:link controller="annotation" action="demo2">Stores annotations using Local Annotator Storage API</g:link></li>
            <li><g:link controller="annotation" action="demo3">Stores annotations using annotateit.org Storage API</g:link></li>
            --%>
            <li>
                <g:link controller="annotation" action="demo4">Basic demo using Catch Storage API</g:link>
                (token generated in javascript on the page)
            </li>
        </ul>
        <h2>Video Annotator</h2>
        <ul>
            <li>
                <g:link controller="annotation" action="demo6">Video demo using Catch Storage API</g:link>
                (does not require authentication)
            </li>
            <li>
                <g:link controller="annotation" action="demo7">Video demo using Catch Storage API</g:link>
                (requires authentication)
            </li>
            <li>
                <g:link controller="annotation" action="demo8">Video demo using Catch Storage API with new URI</g:link>
                (does not require authentication)
            </li>

        </ul>
    </div>
</div>

</body>
</html>