<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>annotateme demo</title>
    <meta name="layout" content="main"/>
    <r:require modules="annotator"/>
</head>
<body>
<div id="content">
<h1>OpenShakespeare annotation demo</h1>
<%--
<div id="controls">
    <h3>Controls</h3>
    <p>
        <input id="username" value="Mark">
    </p>
</div>
--%>
<div id="text-to-annotate">
<h2>The Comedy of Errors</h2>
<h3 class="shkspr-act-title">ACT I</h3>
<h4 class="shkspr-scene-title">SCENE I.  A hall in DUKE SOLINUS'S palace.</h4>
<p class="shkspr-stagedir">Enter DUKE SOLINUS, AEGEON, Gaoler, Officers, and other
Attendants</p>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">AEGEON</p>
    <p class="shkspr-speech-body">Proceed, Solinus, to procure my fall<br />
        And by the doom of death end woes and all.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DUKE SOLINUS</p>
    <p class="shkspr-speech-body">Merchant of Syracuse, plead no more;<br />
        I am not partial to infringe our laws:<br />
        The enmity and discord which of late<br />
        Sprung from the rancorous outrage of your duke<br />
        To merchants, our well-dealing countrymen,<br />
        Who wanting guilders to redeem their lives<br />
        Have seal'd his rigorous statutes with their bloods,<br />
        Excludes all pity from our threatening looks.<br />
        For, since the mortal and intestine jars<br />
        'Twixt thy seditious countrymen and us,<br />
        It hath in solemn synods been decreed<br />
        Both by the Syracusians and ourselves,<br />
        To admit no traffic to our adverse towns Nay, more,<br />
        If any born at Ephesus be seen<br />
        At any Syracusian marts and fairs;<br />
        Again: if any Syracusian born<br />
        Come to the bay of Ephesus, he dies,<br />
        His goods confiscate to the duke's dispose,<br />
        Unless a thousand marks be levied,<br />
        To quit the penalty and to ransom him.<br />
        Thy substance, valued at the highest rate,<br />
        Cannot amount unto a hundred marks;<br />
        Therefore by law thou art condemned to die.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">AEGEON</p>
    <p class="shkspr-speech-body">Yet this my comfort: when your words are done,<br />
        My woes end likewise with the evening sun.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DUKE SOLINUS</p>
    <p class="shkspr-speech-body">Well, Syracusian, say in brief the cause<br />
        Why thou departed'st from thy native home<br />
        And for what cause thou camest to Ephesus.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DUKE SOLINUS</p>
    <p class="shkspr-speech-body">Nay, forward, old man; do not break off so;<br />
        For we may pity, though not pardon thee.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">AEGEON</p>
    <p class="shkspr-speech-body">O, had the gods done so, I had not now<br />
        Worthily term'd them merciless to us!<br />
        For, ere the ships could meet by twice five leagues,<br />
        We were encounterd by a mighty rock;<br />
        Which being violently borne upon,<br />
        Our helpful ship was splitted in the midst;<br />
        So that, in this unjust divorce of us,<br />
        Fortune had left to both of us alike<br />
        What to delight in, what to sorrow for.<br />
        Her part, poor soul! seeming as burdened<br />
        With lesser weight but not with lesser woe,<br />
        Was carried with more speed before the wind;<br />
        And in our sight they three were taken up<br />
        By fishermen of Corinth, as we thought.<br />
        At length, another ship had seized on us;<br />
        And, knowing whom it was their hap to save,<br />
        Gave healthful welcome to their shipwreck'd guests;<br />
        And would have reft the fishers of their prey,<br />
        Had not their bark been very slow of sail;<br />
        And therefore homeward did they bend their course.<br />
        Thus have you heard me sever'd from my bliss;<br />
        That by misfortunes was my life prolong'd,<br />
        To tell sad stories of my own mishaps.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DUKE SOLINUS</p>
    <p class="shkspr-speech-body">And for the sake of them thou sorrowest for,<br />
        Do me the favour to dilate at full<br />
        What hath befall'n of them and thee till now.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">AEGEON</p>
    <p class="shkspr-speech-body">My youngest boy, and yet my eldest care,<br />
        At eighteen years became inquisitive<br />
        After his brother: and importuned me<br />
        That his attendant--so his case was like,<br />
        Reft of his brother, but retain'd his name--<br />
        Might bear him company in the quest of him:<br />
        Whom whilst I labour'd of a love to see,<br />
        I hazarded the loss of whom I loved.<br />
        Five summers have I spent in furthest Greece,<br />
        Roaming clean through the bounds of Asia,<br />
        And, coasting homeward, came to Ephesus;<br />
        Hopeless to find, yet loath to leave unsought<br />
        Or that or any place that harbours men.<br />
        But here must end the story of my life;<br />
        And happy were I in my timely death,<br />
        Could all my travels warrant me they live.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DUKE SOLINUS</p>
    <p class="shkspr-speech-body">Hapless AEgeon, whom the fates have mark'd<br />
        To bear the extremity of dire mishap!<br />
        Now, trust me, were it not against our laws,<br />
        Against my crown, my oath, my dignity,<br />
        Which princes, would they, may not disannul,<br />
        My soul would sue as advocate for thee.<br />
        But, though thou art adjudged to the death<br />
        And passed sentence may not be recall'd<br />
        But to our honour's great disparagement,<br />
        Yet I will favour thee in what I can.<br />
        Therefore, merchant, I'll limit thee this day<br />
        To seek thy life by beneficial help:<br />
        Try all the friends thou hast in Ephesus;<br />
        Beg thou, or borrow, to make up the sum,<br />
        And live; if no, then thou art doom'd to die.<br />
        Gaoler, take him to thy custody.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">Gaoler</p>
    <p class="shkspr-speech-body">I will, my lord.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">AEGEON</p>
    <p class="shkspr-speech-body">Hopeless and helpless doth AEgeon wend,<br />
        But to procrastinate his lifeless end.<br />
    </p>
</div>
<p class="shkspr-stagedir">Exeunt</p>
<h4 class="shkspr-scene-title">SCENE II.  The Mart.</h4>
<p class="shkspr-stagedir">Enter ANTIPHOLUS of Syracuse, DROMIO of Syracuse,
and First Merchant</p>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">First Merchant</p>
    <p class="shkspr-speech-body">Therefore give out you are of Epidamnum,<br />
        Lest that your goods too soon be confiscate.<br />
        This very day a Syracusian merchant<br />
        Is apprehended for arrival here;<br />
        And not being able to buy out his life<br />
        According to the statute of the town,<br />
        Dies ere the weary sun set in the west.<br />
        There is your money that I had to keep.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">Go bear it to the Centaur, where we host,<br />
        And stay there, Dromio, till I come to thee.<br />
        Within this hour it will be dinner-time:<br />
        Till that, I'll view the manners of the town,<br />
        Peruse the traders, gaze upon the buildings,<br />
        And then return and sleep within mine inn,<br />
        For with long travel I am stiff and weary.<br />
        Get thee away.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF SYRACUSE</p>
    <p class="shkspr-speech-body">Many a man would take you at your word,<br />
        And go indeed, having so good a mean.<br />
    </p>
</div>
<p class="shkspr-stagedir">Exit</p>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">A trusty villain, sir, that very oft,<br />
        When I am dull with care and melancholy,<br />
        Lightens my humour with his merry jests.<br />
        What, will you walk with me about the town,<br />
        And then go to my inn and dine with me?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">First Merchant</p>
    <p class="shkspr-speech-body">I am invited, sir, to certain merchants,<br />
        Of whom I hope to make much benefit;<br />
        I crave your pardon. Soon at five o'clock,<br />
        Please you, I'll meet with you upon the mart<br />
        And afterward consort you till bed-time:<br />
        My present business calls me from you now.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">Farewell till then: I will go lose myself<br />
        And wander up and down to view the city.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">First Merchant</p>
    <p class="shkspr-speech-body">Sir, I commend you to your own content.<br />
    </p>
</div>
<p class="shkspr-stagedir">Exit</p>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">He that commends me to mine own content<br />
        Commends me to the thing I cannot get.<br />
        I to the world am like a drop of water<br />
        That in the ocean seeks another drop,<br />
        Who, falling there to find his fellow forth,<br />
        Unseen, inquisitive, confounds himself:<br />
        So I, to find a mother and a brother,<br />
        In quest of them, unhappy, lose myself.<br />
        Here comes the almanac of my true date.<br />
        What now? how chance thou art return'd so soon?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Return'd so soon! rather approach'd too late:<br />
        The capon burns, the pig falls from the spit,<br />
        The clock hath strucken twelve upon the bell;<br />
        My mistress made it one upon my cheek:<br />
        She is so hot because the meat is cold;<br />
        The meat is cold because you come not home;<br />
        You come not home because you have no stomach;<br />
        You have no stomach having broke your fast;<br />
        But we that know what 'tis to fast and pray<br />
        Are penitent for your default to-day.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">Stop in your wind, sir: tell me this, I pray:<br />
        Where have you left the money that I gave you?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">O,--sixpence, that I had o' Wednesday last<br />
        To pay the saddler for my mistress' crupper?<br />
        The saddler had it, sir; I kept it not.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">I am not in a sportive humour now:<br />
        Tell me, and dally not, where is the money?<br />
        We being strangers here, how darest thou trust<br />
        So great a charge from thine own custody?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">I pray you, air, as you sit at dinner:<br />
        I from my mistress come to you in post;<br />
        If I return, I shall be post indeed,<br />
        For she will score your fault upon my pate.<br />
        Methinks your maw, like mine, should be your clock,<br />
        And strike you home without a messenger.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">Come, Dromio, come, these jests are out of season;<br />
        Reserve them till a merrier hour than this.<br />
        Where is the gold I gave in charge to thee?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">To me, sir? why, you gave no gold to me.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">Come on, sir knave, have done your foolishness,<br />
        And tell me how thou hast disposed thy charge.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">My charge was but to fetch you from the mart<br />
        Home to your house, the Phoenix, sir, to dinner:<br />
        My mistress and her sister stays for you.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">In what safe place you have bestow'd my money,<br />
        Or I shall break that merry sconce of yours<br />
        That stands on tricks when I am undisposed:<br />
        Where is the thousand marks thou hadst of me?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">I have some marks of yours upon my pate,<br />
        Some of my mistress' marks upon my shoulders,<br />
        But not a thousand marks between you both.<br />
        If I should pay your worship those again,<br />
        Perchance you will not bear them patiently.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">Thy mistress' marks? what mistress, slave, hast thou?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Your worship's wife, my mistress at the Phoenix;<br />
        She that doth fast till you come home to dinner,<br />
        And prays that you will hie you home to dinner.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">What, wilt thou flout me thus unto my face,<br />
        Being forbid? There, take you that, sir knave.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">What mean you, sir? for God's sake, hold your hands!<br />
        Nay, and you will not, sir, I'll take my heels.<br />
    </p>
</div>
<p class="shkspr-stagedir">Exit</p>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ANTIPHOLUS OF SYRACUSE</p>
    <p class="shkspr-speech-body">Upon my life, by some device or other<br />
        The villain is o'er-raught of all my money.<br />
        They say this town is full of cozenage,<br />
        As, nimble jugglers that deceive the eye,<br />
        Dark-working sorcerers that change the mind,<br />
        Soul-killing witches that deform the body,<br />
        Disguised cheaters, prating mountebanks,<br />
        And many such-like liberties of sin:<br />
        If it prove so, I will be gone the sooner.<br />
        I'll to the Centaur, to go seek this slave:<br />
        I greatly fear my money is not safe.<br />
    </p>
</div>
<p class="shkspr-stagedir">Exit</p>
<h3 class="shkspr-act-title">ACT II</h3><h4 class="shkspr-scene-title">SCENE I.  The house of ANTIPHOLUS of Ephesus.</h4>
<p class="shkspr-stagedir">Enter ADRIANA and LUCIANA</p>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Neither my husband nor the slave return'd,<br />
        That in such haste I sent to seek his master!<br />
        Sure, Luciana, it is two o'clock.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Perhaps some merchant hath invited him,<br />
        And from the mart he's somewhere gone to dinner.<br />
        Good sister, let us dine and never fret:<br />
        A man is master of his liberty:<br />
        Time is their master, and, when they see time,<br />
        They'll go or come: if so, be patient, sister.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Why should their liberty than ours be more?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Because their business still lies out o' door.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Look, when I serve him so, he takes it ill.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">O, know he is the bridle of your will.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">There's none but asses will be bridled so.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Why, headstrong liberty is lash'd with woe.<br />
        There's nothing situate under heaven's eye<br />
        But hath his bound, in earth, in sea, in sky:<br />
        The beasts, the fishes, and the winged fowls,<br />
        Are their males' subjects and at their controls:<br />
        Men, more divine, the masters of all these,<br />
        Lords of the wide world and wild watery seas,<br />
        Indued with intellectual sense and souls,<br />
        Of more preeminence than fish and fowls,<br />
        Are masters to their females, and their lords:<br />
        Then let your will attend on their accords.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">This servitude makes you to keep unwed.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Not this, but troubles of the marriage-bed.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">But, were you wedded, you would bear some sway.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Ere I learn love, I'll practise to obey.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">How if your husband start some other where?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Till he come home again, I would forbear.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Patience unmoved! no marvel though she pause;<br />
        They can be meek that have no other cause.<br />
        A wretched soul, bruised with adversity,<br />
        We bid be quiet when we hear it cry;<br />
        But were we burdened with like weight of pain,<br />
        As much or more would we ourselves complain:<br />
        So thou, that hast no unkind mate to grieve thee,<br />
        With urging helpless patience wouldst relieve me,<br />
        But, if thou live to see like right bereft,<br />
        This fool-begg'd patience in thee will be left.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Well, I will marry one day, but to try.<br />
        Here comes your man; now is your husband nigh.<br />
    </p>
</div>
<p class="shkspr-stagedir">Enter DROMIO of Ephesus</p>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Say, is your tardy master now at hand?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Nay, he's at two hands with me, and that my two ears<br />
        can witness.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Say, didst thou speak with him? know'st thou his mind?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Ay, ay, he told his mind upon mine ear:<br />
        Beshrew his hand, I scarce could understand it.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Spake he so doubtfully, thou couldst not feel his meaning?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Nay, he struck so plainly, I could too well feel his<br />
        blows; and withal so doubtfully that I could scarce<br />
        understand them.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">But say, I prithee, is he coming home? It seems he<br />
        hath great care to please his wife.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Why, mistress, sure my master is horn-mad.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Horn-mad, thou villain!<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">I mean not cuckold-mad;<br />
        But, sure, he is stark mad.<br />
        When I desired him to come home to dinner,<br />
        He ask'd me for a thousand marks in gold:<br />
        ''Tis dinner-time,' quoth I; 'My gold!' quoth he;<br />
        'Your meat doth burn,' quoth I; 'My gold!' quoth he:<br />
        'Will you come home?' quoth I; 'My gold!' quoth he.<br />
        'Where is the thousand marks I gave thee, villain?'<br />
        'The pig,' quoth I, 'is burn'd;' 'My gold!' quoth he:<br />
        'My mistress, sir' quoth I; 'Hang up thy mistress!<br />
        I know not thy mistress; out on thy mistress!'<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">LUCIANA</p>
    <p class="shkspr-speech-body">Quoth who?<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Quoth my master:<br />
        'I know,' quoth he, 'no house, no wife, no mistress.'<br />
        So that my errand, due unto my tongue,<br />
        I thank him, I bare home upon my shoulders;<br />
        For, in conclusion, he did beat me there.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Go back again, thou slave, and fetch him home.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Go back again, and be new beaten home?<br />
        For God's sake, send some other messenger.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Back, slave, or I will break thy pate across.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">And he will bless that cross with other beating:<br />
        Between you I shall have a holy head.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">ADRIANA</p>
    <p class="shkspr-speech-body">Hence, prating peasant! fetch thy master home.<br />
    </p>
</div>
<div class="shkspr-speech">
    <p class="shkspr-speech-speaker">DROMIO OF EPHESUS</p>
    <p class="shkspr-speech-body">Am I so round with you as you with me,<br />
        That like a football you do spurn me thus?<br />
        You spurn me hence, and he will spurn me hither:<br />
        If I last in this service, you must case me in leather.<br />
    </p>
</div>
<p class="shkspr-stagedir">Exit</p>

</div>
</div>
<!--
Metadata
${grailsApplication.metadata}

Configuration
${grailsApplication.config}
-->

<script>
    $(function () {

        var content = $('#content').annotator();

        content.annotator('addPlugin', 'Auth', {
            //tokenUrl: '${grailsApplication.config.annotator.auth.tokenUrl}'
            tokenUrl: '${request.contextPath}/auth/token'
        });


        content.annotator('addPlugin', 'Store', {
            // The endpoint of the store on your server.
            //prefix: '/store/endpoint',
            //prefix: 'http://afstore.aws.af.cm/annotator',
            //grailsApplication.config
            prefix: '${grailsApplication.config.annotator.store.prefix}',
            // Attach the uri of the current page to all annotations to allow search.
            annotationData: {
                'uri': '${createLink(action: action, controller: controller, id: id, absolute:true)}'
            },
            urls: {
                // These are the default URLs.
                create:  '/create',
                read:    '/read/:id',
                update:  '/update/:id',
                destroy: '/destroy/:id',
                search:  '/search'
            },
            // This will perform a "search" action rather than "read" when the plugin
            // loads. Will request the last 20 annotations for the current url.
            // eg. /store/endpoint/search?limit=20&uri=http://this/document/only
            loadFromSearch: {
                'limit': 20,
                'uri': '${createLink(action: action, controller: controller, id: id, absolute:true)}'
            }
        });
        content.annotator('addPlugin', 'Tags');
        content.annotator('addPlugin', 'Filter', {
            filters: [
                {
                    label: 'Quote',
                    property: 'quote'
                }
            ]});
        //content.annotator('addPlugin', 'Markdown');
        content.annotator('addPlugin', 'Permissions', {
            user: '<sec:loggedInUserInfo field="username"/>'
        });

    });
</script>
</body>
</html>

