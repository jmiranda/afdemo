package com.oauthclient



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(OAuthIDController)
class OAuthIDControllerTests {

    void testSomething() {
        fail "Implement me"
    }
}
