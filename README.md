# Quick Demo

## 1. Configure server

### Download source code
Follow instructions from the [README.md](https://github.com/annotationsatharvard/catcha/blob/master/README.md).

### Switch to Grails 2.2.1
```
$ gvm use grails 2.2.1
```
### Start the Catch REST API server 
```
$ grails -Dserver.port=8080 run-app
```
### Create new client API key 
* Go to http://localhost:8080/catch/dashboard/index
* Login as `admin`:`password`
* Click the **Create System** button
* Enter name, short name, and description 
* Click the **Save system** button
* Copy the **API Key** on the next page  
---
## 2. Configure client

### Download Source Code
```
$ git clone https://jmiranda@bitbucket.org/jmiranda/afdemo.git
$ cd afdemo
```
### Switch to Grails 2.2.3
```
$ gvm use grails 2.2.3
```
### Make sure it set to the host and port of the REST API you configured above (`afdemo-config.properties`)
```
# Annotator API URLs
annotator.store.prefix = http://localhost:8080/catch/annotator
annotator.auth.tokenUrl = http://localhost:8080/catch/annotator/token

# Annotator JWT properties (e.g. My First App from CATCH API Systems)
annotator.auth.userId = jmiranda
annotator.auth.consumerKey = 0cbfa370-b73c-4e3a-ae46-582df284b7c3
```

### Start the REST API client
```
$ grails -Dserver.port=8081 run-app
```
---
## 3. Generate a new token (just for testing to make sure the configuration is good)
```
$ curl http://localhost:8081/afdemo/auth/token
eyJhbGciOiJIUzI1NiIsImN0eSI6InRleHRcL3BsYWluIiwidHlwIjoiSldTIn0.eyJjb25zdW1lcktleSI6IjAwOWEwYjFlLTMzY2ItNDNiMi1iMjMyLWMxNTY2NmNjZDc1NCIsImlzc3VlZEF0IjoiMjAxNC0xMC0wN1QxMTowMzozNS0wNDAwIiwidXNlcklkIjoiam1pcmFuZGEiLCJqdGkiOiI1MzA0ZmMzMi1kZjEzLTQyZTAtYTJjYi01MjVhOTY1ODY0NmUiLCJ0dGwiOjg2NDAwLCJpYXQiOjE0MTI3Mzc0MTV9.vCf28YyQaeMPH3vWro19FK8tj6LnWz-lxidJnhljDTM
```
NOTE:  You can use Chrome if you don't have `curl` available.  Enter "view-source:http://localhost:8081/afdemo/auth/token" into the URL field.

---
## 4. Test newly generated token in Google's JWT Decorder
* Go to https://developers.google.com/wallet/digital/docs/jwtdecoder
* Paste the token from the previous step (yours, not the one above) where it says **Paste your JWT below**
* You should see values that match the configuration in `afdemo-config.properties`
```
Header
{
    "alg": "HS256", 
    "typ": "JWS", 
    "cty": "text/plain"
}
Claims
{
    "userId": "jmiranda", 
    "consumerKey": "009a0b1e-33cb-43b2-b232-c15666ccd754", 
    "jti": "5304fc32-df13-42e0-a2cb-525a9658646e", 
    "ttl": 86400, 
    "issuedAt": "2014-10-07T11:03:35-0400", 
    "iat": 1412737415
}
Signature (encoded)
vCf28YyQaeMPH3vWro19FK8tj6LnWz-lxidJnhljDTM
```
---
## 5. Run through some of the demos
* Go to http://localhost:8081/afdemo/annotation/index
* Try your luck with any of the demos
---
## 6. Check the server logs on the REST API
You can view all request information by checking the logs.  By default, we log all requests (method, uri, body, headers) so you can look there if you have any questions.  You should also be able to quickly create your own requests using these as examples.
```
2014-10-07 23:09:47,124 [http-bio-8080-exec-2] INFO  httplogger.DefaultHttpLogger  - << #20 POST http://localhost:8080/catch/annotator/create
2014-10-07 23:09:47,125 [http-bio-8080-exec-2] INFO  httplogger.DefaultHttpLogger  - << #20 headers [cookie: null, host: localhost:8080, connection: keep-alive, accept: application/json, text/javascript, */*; q=0.01, accept-language: en-US,en;q=0.8, accept-encoding: gzip,deflate, origin: http://localhost:8081, x-annotator-auth-token: ********, user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36, content-type: application/json; charset=UTF-8, referer: http://localhost:8081/afdemo/annotation/demo1, x-requested-with: null, x-crsftoken: null, content-type: application/json; charset=UTF-8, content-length: 307]
2014-10-07 23:09:47,125 [http-bio-8080-exec-2] INFO  httplogger.DefaultHttpLogger  - << #20 body: '{"permissions":{"read":[],"update":[],"delete":[],"admin":[]},"user":"jmiranda","text":"woe","tags":[],"ranges":[{"start":"/div[1]/div[1]/p[2]","startOffset":50,"end":"/div[1]/div[2]/p[1]","endOffset":0}],"quote":"y the doom of death end woes and all.","uri":"http://localhost:8081/afdemo/annotation/index"}'
2014-10-07 23:09:47,135 [http-bio-8080-exec-2] INFO  persistence.AnnotatorService  - Create annotation: [tags:[], text:woe, quote:y the doom of death end woes and all., ranges:[[endOffset:0, start:/div[1]/div[1]/p[2], end:/div[1]/div[2]/p[1], startOffset:50]], permissions:[update:[], admin:[], delete:[], read:[]], uri:http://localhost:8081/afdemo/annotation/index, user:jmiranda]
2014-10-07 23:09:47,152 [http-bio-8080-exec-2] INFO  httplogger.DefaultHttpLogger  - >> #20 returned 200, took 28 ms.
2014-10-07 23:09:47,152 [http-bio-8080-exec-2] INFO  httplogger.DefaultHttpLogger  - >> #20 responded with '{"tags":[],"text":"woe","totalComments":0,"ranges":[{"endOffset":0,"start":"/div[1]/div[1]/p[2]","end":"/div[1]/div[2]/p[1]","startOffset":50}],"deleted":false,"uri":"http://localhost:8081/afdemo/annotation/index","id":7,"archived":false,"updated":"2014-10-07T11:09:47.139-0400","created":"2014-10-07T11:09:47.139-0400","quote":"y the doom of death end woes and all.","permissions":{"update":[],"admin":[],"delete":[],"read":[]},"user":"jmiranda"}'
```
---
## 7. Attempt to make requests on your own

### Generate a new token using the client app
```
$ curl http://localhost:8081/afdemo/auth/token
eyJhbGciOiJIUzI1NiIsImN0eSI6InRleHRcL3BsYWluIiwidHlwIjoiSldTIn0.eyJjb25zdW1lcktleSI6IjAwOWEwYjFlLTMzY2ItNDNiMi1iMjMyLWMxNTY2NmNjZDc1NCIsImlzc3VlZEF0IjoiMjAxNC0xMC0wN1QxMToxNDozMi0wNDAwIiwidXNlcklkIjoiam1pcmFuZGEiLCJqdGkiOiJkOGQyOTdhYS00NmJlLTRlMDgtYThjYy1jYjEyZDZmNGQ0NWEiLCJ0dGwiOjg2NDAwLCJpYXQiOjE0MTI3MzgwNzJ9.zgOkDCiOow7b8hWc70lt9A_qgOSAp1R7bgNhnU0aXug
```
### Retrieve all annotations for a given URI
**Request**
```
$ curl -X GET 'http://localhost:8080/catch/annotator/search?limit=20&uri=http%3A%2F%2Flocalhost%3A8081%2Fafdemo%2Fannotation%2Findex' \
-H "x-annotator-auth-token:eyJhbGciOiJIUzI1NiIsImN0eSI6InRleHRcL3BsYWluIiwidHlwIjoiSldTIn0.eyJjb25zdW1lcktleSI6IjAwOWEwYjFlLTMzY2ItNDNiMi1iMjMyLWMxNTY2NmNjZDc1NCIsImlzc3VlZEF0IjoiMjAxNC0xMC0wN1QxMToxNDozMi0wNDAwIiwidXNlcklkIjoiam1pcmFuZGEiLCJqdGkiOiJkOGQyOTdhYS00NmJlLTRlMDgtYThjYy1jYjEyZDZmNGQ0NWEiLCJ0dGwiOjg2NDAwLCJpYXQiOjE0MTI3MzgwNzJ9.zgOkDCiOow7b8hWc70lt9A_qgOSAp1R7bgNhnU0aXug"
```
**Response**
```
{"total":2,"limit":"20","offset":0,"rows":[{"tags":[],"text":"woe","totalComments":0,"ranges":[{"endOffset":0,"start":"/div[1]/div[1]/p[2]","end":"/div[1]/div[2]/p[1]","startOffset":50}],"deleted":false,"uri":"http://localhost:8081/afdemo/annotation/index","id":7,"archived":false,"updated":"2014-10-07T11:09:47.0-0400","created":"2014-10-07T11:09:47.0-0400","quote":"y the doom of death end woes and all.","permissions":{"update":[],"admin":[],"delete":[],"read":[]},"user":"jmiranda"},{"id":5,"tags":[],"text":"sfsafas","archived":false,"totalComments":0,"created":"2014-10-07T05:20:55.0-0400","updated":"2014-10-07T05:20:55.0-0400","quote":"ribus ex eum, eu probo facer eum! Vidit dicant pro no, mel et malis a","ranges":[{"endOffset":213,"start":"/div[1]/p[1]","end":"/div[1]/p[1]","startOffset":144}],"deleted":false,"uri":"http://localhost:8081/afdemo/annotation/index"}]}
```
### Create a new annotation
**Request**
```
curl -X POST http://localhost:8080/catch/annotator/create \
-H "x-annotator-auth-token:eyJhbGciOiJIUzI1NiIsImN0eSI6InRleHRcL3BsYWluIiwidHlwIjoiSldTIn0.eyJjb25zdW1lcktleSI6IjAwOWEwYjFlLTMzY2ItNDNiMi1iMjMyLWMxNTY2NmNjZDc1NCIsImlzc3VlZEF0IjoiMjAxNC0xMC0wN1QxMToxNDozMi0wNDAwIiwidXNlcklkIjoiam1pcmFuZGEiLCJqdGkiOiJkOGQyOTdhYS00NmJlLTRlMDgtYThjYy1jYjEyZDZmNGQ0NWEiLCJ0dGwiOjg2NDAwLCJpYXQiOjE0MTI3MzgwNzJ9.zgOkDCiOow7b8hWc70lt9A_qgOSAp1R7bgNhnU0aXug" \
-H "Content-Type: application/json" \
-d '{"permissions":{"read":[],"update":[],"delete":[],"admin":[]},"user":"jmiranda","text":"woe","tags":[],"ranges":[{"start":"/div[1]/div[1]/p[2]","startOffset":50,"end":"/div[1]/div[2]/p[1]","endOffset":0}],"quote":"y the doom of death end woes and all.","uri":"http://localhost:8081/afdemo/annotation/index"}' 
```
**Response**
```
{"tags":[],"text":"woe","totalComments":0,"ranges":[{"endOffset":0,"start":"/div[1]/div[1]/p[2]","end":"/div[1]/div[2]/p[1]","startOffset":50}],"deleted":false,"uri":"http://localhost:8081/afdemo/annotation/index","id":10,"archived":false,"updated":"2014-10-07T11:26:13.571-0400","created":"2014-10-07T11:26:13.571-0400","quote":"y the doom of death end woes and all.","permissions":{"update":[],"admin":[],"delete":[],"read":[]},"user":"jmiranda"}
```